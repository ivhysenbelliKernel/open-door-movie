<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if($_SERVER['SERVER_NAME'] === "local.opendoor-movie.com"){

define('DB_NAME', 'opendoor');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('WP_HOME','http://local.opendoor-movie.com/');
define('WP_SITEURL','http://local.opendoor-movie.com/');
}
else{
define('DB_NAME', 'opendoor-33354258');

/** MySQL database username */
define('DB_USER', 'opendoor-33354258');

/** MySQL database password */
define('DB_PASSWORD', 'u9ttvmy3f3');

/** MySQL hostname */
define('DB_HOST', 'shareddb-i.hosting.stackcp.net');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('WP_HOME','https://www.opendoor-movie.com/');
define('WP_SITEURL','https://www.opendoor-movie.com/');

}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8t/b.uNSocUlACLk4c Wokr:*l9d6{}}lX0]5np~|HSdO){cwlC4&gS_[EpN/Xhg');
define('SECURE_AUTH_KEY',  'Xmg;}e2?`B2_s92Ph~5_ xl2zF(s7[@FKI8h#RIW*VgM[]s[KE.IHXc-$x%n#E!X');
define('LOGGED_IN_KEY',    '80^Q=MqC]u-aS7PGP*0[QpuKw<;md LS:Q^z|=;d{X,!8~HSAHO8wD4&1F#hd,e.');
define('NONCE_KEY',        'xRm9gW{.xtni]cyq`m,h79[U*tvAD73OII#]nPY!aBVmyRI9u*W(W+(SN$OyNP#.');
define('AUTH_SALT',        'ex0mm7a8=3-8^fnV<ON%4OA1MPufo_3i*!9-]OD@fZ`:pK7XdsikFD-bA}<];w!V');
define('SECURE_AUTH_SALT', 'F:uu0!Tat756YFF4e!sH9ZVH(R.zip5el^uXf<G#xGPEjgV!.nIu#&61Yo$&&J.L');
define('LOGGED_IN_SALT',   'bWbjEfz;/};ZC)Y>=E)4CtmR7xO2k(FwvK`Dp CMqj;>-^DhF#2_PzGz1.8s#=[F');
define('NONCE_SALT',       'o3hiG<ykR];EH+qwDJeMa1x1Mg]~i-O8]0_R_Sf(6v=^ihdDImMtvi#HYdbFk(6=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

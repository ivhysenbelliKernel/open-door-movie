<?php
/**
 * The template for displaying Front-Page
 *
 * Template name: Homepage
 *
 * @package OpenDoor
 */
get_header();

$mainBanner = get_field('main_banner');
$synopsisBanner = get_field('synopsis_banner');
$castBanner = get_field('cast_section_banner');
$cast = get_field('cast');
$filmMakersBanner = get_field('filmmakers_section_banner');
$filmMakers = get_field('filmmakers');
$images = get_field('gallery_images');
$newsBanner = get_field('news_section_banner');
$newsURL = get_field('news_section_url');
$logoCarousel = get_field('supporter_logos_carousel');
$supportersBanner = get_field('supporters_banner');
$supportersTitle = get_field('supporters_title');
?>
<div class="container-fluid">
	<div class="row">
		<section id="home" class="main-banner " style="background-image: url('<?php echo $mainBanner['url']; ?>')">
			<div class="overlay-to-bottom"></div>
			<div class="content">
				<h1><?php echo(get_field('banner_title')); ?></h1>
				<h4><?php echo(get_field('banner')); ?></h4>
			</div>
		</section>
		<section id="synopsis" class="synopsis" style="background-image: url('<?php echo $synopsisBanner['url']; ?>')">
			<div class="overlay-to-right"></div>
			<div class="container">
				<div class="row">
			<div class="content">
				<h1><?php echo(get_field('synopsis_title')); ?></h1>
				<p><?php echo(get_field('synopsis_text')); ?></p>
			</div>
		</div>
	</div>
		</section>

		<section id="cast" class="cast-section" style="background-image: url('<?php echo $castBanner['url']; ?>')">
			<div class="container">
				<div class="row">
			<div class="content">
				<div class="title"><h1><?php echo (get_field('cast_section_title')); ?></h1></div>
					<?php foreach ($cast as $actor) { ?>
						<div class="cast">
							<p><?php echo $actor['role']; ?></p>
							<h6><?php echo $actor['actor']; ?></h6>
						</div>
					<?php } ?>
			</div>
			</div>
		</section>

		<section id="filmmakers" class="filmmaker" style="background-image: url('<?php echo $filmMakersBanner['url']; ?>')">
			<div class="overlay-to-bottom"></div>
			<div class="container">
				<div class="row">
			<div class="content content-right">
				<div class="title"><h1><?php echo (get_field('filmmaker_section_title')); ?></h1></div>
					<?php foreach ($filmMakers as $filmmaker) { ?>
						<div class="filmmakers">
							<p><?php echo $filmmaker['role']; ?></p>
							<h6><?php echo $filmmaker['name']; ?></h6>
						</div>
					<?php } ?>
			</div>
			</div>
		</section>
		<section id="gallery" class="gallery">
			<div id="filmGallery" class="carousel slide" data-ride="carousel">
				  <ul class="carousel-indicators">
                            <?php $index = 0;
                                foreach ($images as $image) { ?>
                                <li data-target="#filmGallery" data-slide-to="<?php echo $index; ?>" class="<?php if($index == 0){echo 'active';} ?>"></li>
                            <?php  $index++; } ?>
                              </ul>
				  <div class="carousel-inner">
				  	<?php 
				  	$index = 0;
				  	foreach ($images as $image) { ?>
				  	<div class="carousel-item <?php if($index == 0) {echo 'active';} ?>">
				      <img class="d-block w-100" src="<?php echo $image['url']; ?>" alt="First slide">
				    </div>
				  	<?php 
				  	$index += 1;
				  	} ?>
				  </div>
				  <div class="overlay-to-right"></div>
				  <div class="content">
				  	<h1><?php echo (get_field('gallery_section_title')) ?></h1>
				  </div>
				  <a class="carousel-control-prev" href="#filmGallery" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				  </a>
				  <a class="carousel-control-next" href="#filmGallery" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				  </a>
				</div>
		</section>
		<section id="news" class="news" style="background-image: url('<?php echo $newsBanner['url']; ?>')">
				<div class="container">
					<div class="row">
						<div class="content">
							<div class="title"><h1><?php echo (get_field('news_section_title')); ?></h1></div>
						</div>
						<div class="container-fluid news-container">
							<div class="row">
								<?php 	$args = array(
											'numberposts' => 3,
											'orderby' => 'post_date',
											'order' => 'DESC',
											'include' => '',
											'exclude' => '',
											'meta_key' => '',
											'meta_value' =>'',
											'post_type' => 'post',
											'post_status' => 'publish',
											'suppress_filters' => true
										);
							$query =  wp_get_recent_posts( $args, ARRAY_A );
							
							foreach ($query as $singlePost) { ?>
								<div class="col-md-12 col-lg-4 news-col">
							 		<div class="post-card">
							 			<div class="post-excerpt">
							 			<p><?php echo $singlePost['post_excerpt']; ?></p>
							 			</div>
							 			<a href="<?php echo (get_permalink($singlePost['ID'])); ?>"><div class="post-title">
							 				<h3><?php echo $singlePost['post_title'] ?></h3>
							 			</div>
							 			</a>
							 		</div>
							 		</div>
							 <?php } ?>
							</div>
						</div>
						<div class="all-news">
							<a href="<?php echo $newsURL ?>"><h3>SEE ALL</h3></a>
						</div>
					</div>
				</div>
		</section>
		<section class="supporters">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="content">
							<h1><?php echo $supportersTitle; ?></h1>
						</div>
					</div>
				</div>
				<div class="row">
					<?php foreach ($logoCarousel as $logo) {  ?>
						<div class="col-3 col-lg-3 col-flex">
							<img src="<?php echo $logo['logo']['url']; ?>" alt="">
						</div>
					<?php } ?>
				</div>
			</div>
		</section>
		</div>
	</div>
</div>
<?php
get_footer();
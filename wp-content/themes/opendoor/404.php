<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package OpenDoor
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
					<div class="title"><h3>page not found</h3></div>
					<div class="error"><h1>404</h1></div>
					<div class="redirect-home"><h3>redirect to <a href="<?php echo get_home_url(); ?>"> home </a></h3></div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

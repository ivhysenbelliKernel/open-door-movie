(function($) {

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 180) {
            //clearHeader, not clearheader - caps H
            $("header").addClass("scrolled");
        } else {
            $("header").removeClass("scrolled");
        }
    });

    $('.menu-toggle-custom').click(function() {
        $('.menu-toggle-custom').toggleClass('active');
        $('.custom-menu-class').toggleClass('active');
    });

    $('.menu-toggle').click(function() {
        $('.menu-toggle').toggleClass('active');
        $('.menu-primary-menu-homepage-container').toggleClass('active');
    });

    if ($("body").hasClass("page-template-news")) {
        $('a[href=#contacts]').click(function(e) {
            e.preventDefault();

            var id = $(this).attr('href');  
            var scrollTo = $(id).offset().top;

            $('html,body').animate({
                'scrollTop': scrollTo
            }, 750);
        });
}
    
    if ($("body").hasClass("home")) {

        $('a[href*=#]').click(function(e) {
            e.preventDefault();

            var id = $(this).attr('href');
            var scrollTo = $(id).offset().top;

            $('html,body').animate({
                'scrollTop': scrollTo
            }, 750);
        });


        $(document).scroll(function() {
            highlightSection();
        });

        function highlightSection() {
            // Where's the scroll at?
            var currentPosition = $(this).scrollTop();

            // Remove highlights from all
            $('a[href*=#]').removeClass('highlighted');

            // Loop over each section
            $('section').each(function() {
                // Calculate the start and end position of the section
                var sectionStart = $(this).offset().top - 80;
                var sectionEnd = sectionStart + $(this).height();

                // If the current scroll lies between the start and the end of this section
                if (currentPosition >= sectionStart && currentPosition < sectionEnd) {
                    // Highlight the corresponding anchors
                    $('a[href=#' + $(this).attr('id') + ']').addClass('highlighted');
                }
                if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                    $('a[href=#news]').removeClass('highlighted');
                    $('a[href=#colophon]').addClass('highlighted');
                }
            });
        };
        highlightSection();
    }

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scroll-to-top').css('display', 'flex');
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.scroll-to-top').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 800);
        return false;
    });

    window.sr = ScrollReveal({ duration: 1000 });
    if($(window).width() > 991){
            sr.reveal('header ul li', 50);
    }
    sr.reveal('.content');
    sr.reveal('.carousel-indicators li', 80);
    sr.reveal('.post-card', 50);
    sr.reveal('.contacts');
    sr.reveal('.social-links');
    sr.reveal('.post-row > .col-lg-8');
    sr.reveal('.post-row > .col-lg-4');



})(jQuery);
<?php
/**
 * The template for displaying Front-Page
 *
 * Template name: News
 *
 * @package OpenDoor
 */

get_header(); 
$newsBanner = get_field('page_banner');
?>

		<section class="all-news-page" style="background-image: url('<?php echo $newsBanner['url']; ?>')">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-title">
							<div class="content">
								<h1 class="title">
									<?php the_title(); ?>
								</h1>
							</div>
						</div>
					<?php 	$args = array(
											'numberposts' => -1,
											'orderby' => 'post_date',
											'order' => 'DESC',
											'include' => '',
											'exclude' => '',
											'meta_key' => '',
											'meta_value' =>'',
											'post_type' => 'post',
											'post_status' => 'publish',
											'suppress_filters' => true
										);
							$query =  wp_get_recent_posts( $args, ARRAY_A );
							
							foreach ($query as $singlePost) { ?>
								<div class="col-md-12 col-lg-4 news-col">
							 		<div class="post-card">
							 			<div class="post-excerpt">
							 			<p><?php echo $singlePost['post_excerpt']; ?></p>
							 			</div>
							 			<a href="<?php echo (get_permalink($singlePost['ID'])); ?>"><div class="post-title">
							 				<h3><?php echo $singlePost['post_title'] ?></h3>
							 			</div>
							 			</a>
							 		</div>
							 		</div>
							 <?php } ?>
					
				</div>
			</div>
			
		</section>

<?php
get_footer();
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package OpenDoor
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div id="contacts" class="container">
			<div class="row">
				<div class="col-md-12 col-lg-6 col-contacts">
					<div class="content">
						<h1>CONTATCS</h1>
					</div>
					<?php $contacts = get_field('contact_entry','option');?>
					<div class="contacts">
						<?php foreach ($contacts as $contact) { ?>
						<div class="contact">
							<h3><?php echo $contact['contact_name'] ?></h3>
							<h5><?php echo $contact['contact_role'] ?></h5>
							<a href="mailto:<?php echo $contact['contact_e-mail'] ?>"><h5><?php echo $contact['contact_e-mail'] ?></h5></a>
						</div>
						<?php } ?>
					</div>

					<div class="social-links">
						<?php $socialLinks = get_field('social_links','option'); 
								// var_dump($socialLinks);
								foreach ($socialLinks as $social) { ?>
									<a href="<?php echo $social['social_link_url']?>"><i class="fab fa-<?php echo $social['social_link_name'] ?>" aria-hidden="true"></i></a>
								<?php } ?> 

					</div>
				</div>
				<div class="col-md-12 col-lg-6">
					<div class="contact-form">
						<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
	<div class="scroll-to-top">
			<i class="fa fa-angle-up"></i>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package OpenDoor
 */
global $post;
$post = get_post($post);
$pfx_date = get_the_date( 'j F Y' ,$post->ID);
get_header();
?>

	<div class="container-fluid">
		<div class="row post-section">
			<div class="container">
				<div class="row post-row">
					<div class="col-lg-8">
							<?php //var_dump($post);  ?>
							<div class="information-data">
								<a href="<?php echo $post->post_resource_link ?>"><h3 class="resource"><?php echo $post->post_resource_title; ?></h3></a>
								<h3 class="data"><?php echo $pfx_date; ?></h3>
							</div>
							<div class="post-wrapper">
							<div class="post-title">
								<h1><?php echo $post->post_title ?></h1>
							</div>
							<div class="post-content">
								<p><?php echo (wpautop($post->post_content)); ?></p>
							</div>
						</div>
						</div>
						<div class="col-lg-4">
							<div class="sidebar">
							<div class="other-articles-data">
								<h3>OTHER ARTICLES</h3>
							</div>
							<div class="post-cards-collection">
							<?php 	$args = array(
											'numberposts' => 3,
											'orderby' => 'rand',
											'order' => 'DESC',
											'include' => '',
											'exclude' => '',
											'meta_key' => '',
											'meta_value' =>'',
											'post_type' => 'post',
											'post_status' => 'publish',
											'suppress_filters' => true
										);
							$query =  wp_get_recent_posts( $args, ARRAY_A );
							
							foreach ($query as $singlePost) { ?>
								<div class="col-md-12 col-lg-23 news-col">
							 		<div class="post-card">
							 			<div class="post-excerpt">
							 			<p><?php echo $singlePost['post_excerpt']; ?></p>
							 			</div>
							 			<a href="<?php echo (get_permalink($singlePost['ID'])); ?>"><div class="post-title">
							 				<h3><?php echo $singlePost['post_title'] ?></h3>
							 			</div>
							 			</a>
							 		</div>
							 		</div>
							 <?php } ?>
						</div>
						</div>
						</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();
